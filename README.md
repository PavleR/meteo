# METEO

### Know the forecast of the next hours & days.

Developed with Vala & Gtk,using OpenWeatherMap API (https://openweathermap.org/)

![Screenshot](./data/screens/screenshot_1.png  "Meteo")

### Features:

- Current weather, with information about temperature, pressure, wind speed and direction, sunrise & sunset.
- Forecast for next 18 hours.
- Forecast for next five days.
- Choose your units (metric, imperial or british).
- Choose your city, with maps help.
- Awesome maps with weather info.
- System tray indicator.

----

### How To Install

#### For Ubuntu and derivates:

You can add my ppa at Launchpad.net, only for *bionic* releases:

    sudo add-apt-repository ppa:bitseater/ppa
    sudo apt update
    sudo apt install com.gitlab.bitseater.meteo

For **LinuxMint** users, you must to activate appindicator support:

	Configuration -> Preferences -> General -> Activate indicator support (Cinnamon restart required)

#### For Debian and derivates:

You can download the last .deb package from:

[Package: Debian 9](https://gitlab.com/bitseater/meteo/-/jobs/artifacts/master/download?job=package%3Adebian-9)

[Package: Ubuntu 18.04](https://gitlab.com/bitseater/meteo/-/jobs/artifacts/master/download?job=package%3Aubuntu-18.04)

#### Flatpak package:

Also, you can install the flatpak package:

    flatpak install --from https://flathub.org/repo/appstream/com.gitlab.bitseater.meteo.flatpakref

Or download [Package: Flatpak](https://gitlab.com/bitseater/meteo/-/jobs/artifacts/master/download?job=flatpack-package), unzip and install with: 

	flatpak install com.gitlab.bitseater.meteo.flatpakref

#### Snap package:

Snap package is available at [Meteo in Snap Store](https://snapcraft.io/meteo)

I've added it at the request of the users, but I don't give support to Snap.You can install it, at your own risk, from terminal:

    sudo snap install meteo

----

### How To Build

Library Dependencies :

- gtk+-3.0
- libsoup-2.4
- json-glib-1.0
- clutter-1.0
- clutter-gtk-1.0
- champlain-0.12
- geocode-glib-1.0
- webkit2gtk-4.0
- appindicator3-0.1
- meson


For advanced users!

    git clone https://gitlab.com/bitseater/meteo.git
    cd meteo
    ./quick.sh -b

----

#### New on release 0.8.4:

- Migration to GitLab.com.

Fixed issues: 98.

----
### Other screenshots:

**A map with temperatures by Dark Sky**
![Screenshot](./data/screens/screenshot_2.png  "Meteo")

**A map with temperatures by OpenWeatherMap:**
![Screenshot](./data/screens/screenshot_3.png  "Meteo")

**Indicator in wingpanel / system tray:**
![Screenshot](./data/screens/screenshot_4.png  "Meteo")
